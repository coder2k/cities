package ca.dieterlunn.games.monopoly;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.snilius.aboutit.AboutIt;
import com.snilius.aboutit.L;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = (Toolbar) findViewById(R.id.aboutToolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setTitle(R.string.app_name);
        ab.setDisplayHomeAsUpEnabled(true);

        new AboutIt(this).app(R.string.app_name)
                .buildInfo(BuildConfig.DEBUG, BuildConfig.VERSION_CODE, BuildConfig.VERSION_NAME)
                .copyright("Dieter Lunn")
                .libLicense("Cities", "Dieter (coder2000) Lunn", L.AP2, "http://dieterlunn.ca/cities")
                .toTextView(R.id.aboutText);
    }
}
