package ca.dieterlunn.games.monopoly;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private ImageView[] builds = new ImageView[4];
    private Handler runCountdown = new Handler();
    private MediaPlayer hammerSound;
    private boolean running;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        hammerSound = MediaPlayer.create(this, R.raw.hammer);

        builds[0] = (ImageView) findViewById(R.id.house1);
        builds[1] = (ImageView) findViewById(R.id.house2);
        builds[2] = (ImageView) findViewById(R.id.house3);
        builds[3] = (ImageView) findViewById(R.id.train);

        resetImages();

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        final ImageButton button = (ImageButton) findViewById(R.id.roll_build);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateHouses();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page")
                .setUrl(Uri.parse("http://dieterlunn.ca/cities"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private void animateHouses() {
        if (running) return;

        running = true;
        Random rand = new Random(System.currentTimeMillis());
        int value = rand.nextInt(16);

        Log.d("Random Value", String.valueOf(value));
        switch (value) {
            case 0:case 4:case 8:case 12:
                hammerSound.start();
                builds[0].setVisibility(View.VISIBLE);
                builds[1].setVisibility(View.INVISIBLE);
                builds[2].setVisibility(View.INVISIBLE);
                builds[3].setVisibility(View.INVISIBLE);
                break;
            case 1:case 5:case 9:case 13:
                hammerSound.start();
                builds[0].setVisibility(View.VISIBLE);
                builds[1].setVisibility(View.VISIBLE);
                builds[2].setVisibility(View.INVISIBLE);
                builds[3].setVisibility(View.INVISIBLE);
                break;
            case 2:case 6:case 10:case 14:
                hammerSound.start();
                builds[0].setVisibility(View.VISIBLE);
                builds[1].setVisibility(View.VISIBLE);
                builds[2].setVisibility(View.VISIBLE);
                builds[3].setVisibility(View.INVISIBLE);
                break;
            case 3:case 7:case 11:case 15:
                builds[0].setVisibility(View.INVISIBLE);
                builds[1].setVisibility(View.INVISIBLE);
                builds[2].setVisibility(View.INVISIBLE);
                builds[3].setVisibility(View.VISIBLE);
                break;
        }

        runCountdown.postDelayed(countdown, 3000);
    }

    private void resetImages() {
        for (ImageView build : builds) build.setVisibility(ImageView.INVISIBLE);
    }

    private Runnable countdown = new Runnable() {
        @Override
        public void run() {
            resetImages();
            hammerSound.stop();
            hammerSound.prepareAsync();

            running = false;

            runCountdown.removeCallbacks(this);
        }
    };
}
